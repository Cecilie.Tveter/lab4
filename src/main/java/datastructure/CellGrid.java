package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    private CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        cellGrid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                cellGrid[i][j] = initialState;
            }

        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }



    @Override
    public void set(int row, int column, CellState element) {
        if (outOfBonds(row, column))
            throw new IndexOutOfBoundsException();
        cellGrid [row][column]= element;
    }


    @Override
    public CellState get(int row, int column) {
        if (outOfBonds(row, column))
            throw new IndexOutOfBoundsException();
        return cellGrid[row][column];
    }

    private boolean outOfBonds (int rows, int columns) {
        if (rows >= 0 && rows <= numRows() && columns >= 0 && columns <= numColumns())
            return false;
        return true;
    }

    @Override
    public IGrid copy() {
        IGrid copyGrid = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                copyGrid.set(i, j, get(i, j));
            }
        }
        return copyGrid;
    }
}

